/**
 * 
 */
package com.springernature.exception;

/**
 * @author Saurav
 * This is an user defined exception class for Ticket API
 */
public class TicketAPIException extends Exception {

	private static final long serialVersionUID = 1L;

	public TicketAPIException() {
		super();
	}

	public TicketAPIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TicketAPIException(String message, Throwable cause) {
		super(message, cause);
	}

	public TicketAPIException(String message) {
		super(message);
	}

	public TicketAPIException(Throwable cause) {
		super(cause);
	}
	
}
