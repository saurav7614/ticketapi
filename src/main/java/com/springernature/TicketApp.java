
package com.springernature;

import com.springernature.exception.TicketAPIException;
import com.springernature.model.RateCard;
import com.springernature.model.RateInfo;
import com.springernature.model.TicketInfo;

/**
 * @author Saurav
 * This class is responsible to print ticket details
 */
public class TicketApp {

	public static void main(String[] args) throws TicketAPIException {

		/* Values are hard-coded as of now could come from API call or could keep in
		 static hash map or static constants, Could create as many objects as we want
		 as of now four trips are created*/

		   TicketInfo ticketInfo = new TicketInfo("Pune", "Nasik", 2, 200);
		// TicketInfo ticketInfo = new TicketInfo("Pune", "Mumbai", 2, 120);
		// TicketInfo ticketInfo = new TicketInfo("Mumbai", "Goa", 2, 350);
		// TicketInfo ticketInfo = new TicketInfo("Mumbai", "Nasik", 2, 180);

		RateCard rateCard = new RateCard() {
			{
				setInitialKms(100);
				setInitialPrice(750);
			}
		};

		RateInfo rateInfo = new RateInfo(rateCard, 5);

		TicketAPI ticketAPI = new TicketAPI();
		Integer totalPrice = ticketAPI.calculatePrice(ticketInfo, rateInfo);

		System.out.println("Taxi Ticket\n" + "---------------- \n" + "Source: " + ticketInfo.getSource() + "\n"
				+ "Destination: " + ticketInfo.getDestination() + "\n" + "Kms: " + ticketInfo.getTotalDistance() + "\n"
				+ "No. of Travellers: " + ticketInfo.getNoOfTravelers() + "\n" + "Total Price: " + totalPrice + " INR");

	}

}
