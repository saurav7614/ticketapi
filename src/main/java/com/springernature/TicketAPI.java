/**
 * 
 */
package com.springernature;

import com.springernature.exception.TicketAPIException;
import com.springernature.model.RateInfo;
import com.springernature.model.TicketInfo;

/**
 * @author Saurav 
 * This class is to calculate total price of the ticket
 */
public class TicketAPI {

	public Integer calculatePrice(TicketInfo ticketInfo, RateInfo rateInfo) throws TicketAPIException {
		try {
			Integer totalPrice = null;

			Integer initialKms = rateInfo.getRateCard().getInitialKms();
			Integer initialPrice = rateInfo.getRateCard().getInitialPrice();

			if (ticketInfo.getTotalDistance() > initialKms) {
				Integer regularKms = ticketInfo.getTotalDistance() - initialKms;
				Integer totalPricePerPassenger = (initialPrice) + (regularKms * rateInfo.getRegularPrice());
				totalPrice = (totalPricePerPassenger * ticketInfo.getNoOfTravelers());
			} else {
				totalPrice = (initialPrice * ticketInfo.getNoOfTravelers());
			}
			return totalPrice;
		} catch (Exception e) {
			e.printStackTrace();
			throw new TicketAPIException("TicketAPIException", e.getCause());
		}
	}
}
