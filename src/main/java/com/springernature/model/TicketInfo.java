package com.springernature.model;

/**
 * @author Saurav 
 * This class contains required ticket information
 *
 */
public class TicketInfo {

	private String source;
	private String destination;
	private Integer noOfTravelers;
	private Integer totalDistance;

	public TicketInfo(String source, String destination, Integer noOfTravelers, Integer totalDistance) {
		super();
		this.source = source;
		this.destination = destination;
		this.noOfTravelers = noOfTravelers;
		this.totalDistance = totalDistance;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getNoOfTravelers() {
		return noOfTravelers;
	}

	public void setNoOfTravelers(Integer noOfTravelers) {
		this.noOfTravelers = noOfTravelers;
	}

	public Integer getTotalDistance() {
		return totalDistance;
	}

	public void setTotalDistance(Integer totalDistance) {
		this.totalDistance = totalDistance;
	}

}

