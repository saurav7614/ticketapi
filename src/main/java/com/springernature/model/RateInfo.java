/**
 * 
 */
package com.springernature.model;

/**
 * @author Saurav
 * This class contains rate information including initial and regular kilometers
 */

public class RateInfo {

	private RateCard rateCard;
	private Integer regularPrice;

	public RateInfo(RateCard rateCard, Integer regularPrice) {
		super();
		this.rateCard = rateCard;
		this.regularPrice = regularPrice;
	}

	public RateCard getRateCard() {
		return rateCard;
	}

	public void setRateCard(RateCard rateCard) {
		this.rateCard = rateCard;
	}

	public Integer getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(Integer regularPrice) {
		this.regularPrice = regularPrice;
	}

}
