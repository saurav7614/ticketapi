/**
 * 
 */
package com.springernature.model;

/**
 * @author Saurav
 * This class is to set up rate card for initial running Kilometers.
 */
public class RateCard {
	
	private Integer initialKms;
	private Integer initialPrice;
	public Integer getInitialKms() {
		return initialKms;
	}
	public void setInitialKms(Integer initialKms) {
		this.initialKms = initialKms;
	}
	public Integer getInitialPrice() {
		return initialPrice;
	}
	public void setInitialPrice(Integer initialPrice) {
		this.initialPrice = initialPrice;
	}
	
}
