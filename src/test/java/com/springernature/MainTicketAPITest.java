package com.springernature;

import com.springernature.exception.TicketAPIException;
import com.springernature.model.RateCard;
import com.springernature.model.RateInfo;
import com.springernature.model.TicketInfo;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for MainTicketAPI.
 */
public class MainTicketAPITest 
    extends TestCase
{
	
	private TicketInfo ticketInfo;
	private RateInfo rateInfo;
	private RateCard rateCard;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MainTicketAPITest( String testName )
    {
        super( testName );
    }
    
    @Override
    protected void setUp() throws Exception {
    	ticketInfo = new TicketInfo("Pune", "Mumbai", 2, 120);
		
		rateCard = new RateCard() {
			{
				setInitialKms(100);
				setInitialPrice(750);
			}
		};
		rateInfo = new RateInfo(rateCard, 5);
    }
    
    // This is a positive test
    public void testCalculatePrice() throws TicketAPIException {
    	System.out.println("Running positive calculatePriceTest()..");
    	TicketAPI ticketAPI = new TicketAPI();
    	String totalPrice = ticketAPI.calculatePrice(ticketInfo, rateInfo).toString();
		assertEquals(totalPrice, "1700");
    }
    
    // This is a negative test
    public void testCalculatePriceNegative() {
		try {
			System.out.println("Running negative calculatePriceTest()..");
			ticketInfo = null;
			TicketAPI ticketAPI = new TicketAPI();
			ticketAPI.calculatePrice(ticketInfo, rateInfo).toString();
		} catch (TicketAPIException e) {
			e.printStackTrace();
			assertEquals(e.getMessage(), "TicketAPIException");
		}
    }


    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MainTicketAPITest.class );
    }

    /**
     * Rigourous Test 
     */
    public void testApp()
    {
        assertTrue( true );
    }
}
