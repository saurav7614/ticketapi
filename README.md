# SpringerNature Ticketing API
This app is to print the ticket information with the hard-coded values set in the objects.

# Getting Started
Pull this project into some directory at your local drive.
Run command "mvn clean install eclipse:clean eclipse:eclipse" 
Build should be successful with the passing test cases.
Once build is successfull, Import as existing java project in your IDE.
Run "TicketApp.java" as Java Application.
Ticket should be printed as below in the console:

			Taxi Ticket
			---------------- 
			Source: Pune
			Destination: Nasik
			Kms: 200
			No. of Travellers: 2
			Total Price: 2500 INR
			
# Prerequisites
JDK 1.8
Maven
Git
Any IDE(IntelliJ/Eclipse/NetBeans)
Bitbucket acoount to pull from my public repo




